import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquilinoPage } from './inquilino.page';

describe('InquilinoPage', () => {
  let component: InquilinoPage;
  let fixture: ComponentFixture<InquilinoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquilinoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquilinoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
