import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import * as firebase from "firebase";
import { Events } from '@ionic/angular';

var Vivienda;
@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.page.html',
  styleUrls: ['./modal-page.page.scss'],
})
export class ModalPagePage implements OnInit {

  id : any;
  familia : any;
  NPuerta : any;
  Nninos : any;
  expensas : any;
  Nmascotas : any;

  constructor(public navCtrl : NavController,public navParams : NavParams,private modalCtrl: ModalController,public events : Events) 
  {
    firebase.database().ref('/edificio/1/1/vivienda/1/').once('value').then(function(snapshot) {
      Vivienda = snapshot.val();
      console.log(Vivienda);
      events.publish('readed',69,69);
    });

    //Damos los datos a las variables locales
    events.subscribe('readed',(user,time) => {
      this.id = Vivienda.numEdificiosRegistrados;
      this.familia = Vivienda.familia;
      this.expensas = Vivienda.pagaExpensas;
      this.NPuerta = Vivienda.numeroPuerta;
      this.Nninos = Vivienda.numNinos;
      this.Nmascotas = Vivienda.numMascotas
    });
  }

  ngOnInit() {

  }
  cerrarModal(){
    this.modalCtrl.dismiss()
  }

}
