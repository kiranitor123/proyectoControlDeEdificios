import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: '', loadChildren: './inicio/inicio.module#InicioPageModule' },
  { path: 'edificio', loadChildren: './edificio/edificio.module#EdificioPageModule' },
  { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
  { path: 'edificios-administrador', loadChildren: './edificios-administrador/edificios-administrador.module#EdificiosAdministradorPageModule' },
  { path: 'edificios-dueno-inquilino', loadChildren: './edificios-dueno-inquilino/edificios-dueno-inquilino.module#EdificiosDuenoInquilinoPageModule' },
  { path: 'detalles-edificio', loadChildren: './detalles-edificio/detalles-edificio.module#DetallesEdificioPageModule' },
  { path: 'modal-page', loadChildren: './modal-page/modal-page.module#ModalPagePageModule' },
  { path: 'inquilino', loadChildren: './inquilino/inquilino.module#InquilinoPageModule' },
  { path: 'tab4', loadChildren: './tab4/tab4.module#Tab4PageModule' }


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
