import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';
import * as firebase from "firebase";
import { Events } from '@ionic/angular';

var Edificio;

@Component({
  selector: 'app-detalles-edificio',
  templateUrl: './detalles-edificio.page.html',
  styleUrls: ['./detalles-edificio.page.scss'],
})
export class DetallesEdificioPage implements OnInit {

Edificio : any;

  constructor(public modalController: ModalController,public events : Events) 
  {

  }
  async detallesVivienda() {
    const modal = await this.modalController.create({
      component: ModalPagePage
    });
    await modal.present();
  }

  ngOnInit() {
  }

}
