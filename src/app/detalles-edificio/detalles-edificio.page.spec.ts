import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesEdificioPage } from './detalles-edificio.page';

describe('DetallesEdificioPage', () => {
  let component: DetallesEdificioPage;
  let fixture: ComponentFixture<DetallesEdificioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesEdificioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesEdificioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
