import { Component, OnInit } from '@angular/core';
import { NavController, IonImg } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from '@ionic/angular';
import * as firebase from "firebase";

import { Events } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

var Usuario;

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  def : any = "/assets/img/defaultUser.png";
  texto : any = '';

  email : any;
  password : any;
  duen : any;
  id : any;
  
  firebaseImage : any;
  filename : any;
  nombreImagen : any;
  urlImagen : any;

  condicional : Boolean = true;
  condicional1: Boolean = false;
  condicional2 : Boolean = false;
  condicional3 : Boolean = false;

  correo : any;

  constructor(public events: Events, public navCtrl: NavController,private afAuth: AngularFireAuth,
    private router: Router,public loadingController: LoadingController, public alertCtrl : AlertController,
    private camera : Camera) {
      firebase.database().ref('/usuario/').once('value').then(function(snapshot) {
        Usuario = snapshot.val();
        console.log("Lectura Realizada");
        events.publish('readed',69,69);
      });
  
      //Damos los datos a las variables locales
      events.subscribe('readed',(user,time) => {
        this.id = Usuario.cantidadAdministradores;
      });
    }
    
    ngOnInit() {
    }

    private optionsGallery: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
     };

    funcionBontones(aux : number){
      if(aux==1){
        this.condicional1 = true;
        this.condicional2 = false;
        this.condicional3 = false;
      }
      else{
        if(aux==2){
          this.condicional2 = true;
          this.condicional1 = false;
          this.condicional3 = false;
        }
        else{
          this.condicional3 = true;
          this.condicional1 = false;
          this.condicional2 = false;
        }
      }
    }

    actualizar(ac : any){
      firebase.database().ref('/usuario/CantidadAdmins/').set({
        cantidadAdministradores : ac,
      });
    }

    encriptado(){
      this.correo = this.email;
          this.correo = this.correo.replace(/\@/gi,"83nv6sl9");
          this.correo = this.correo.replace(/\./gi,"v88f73n3");
          this.correo = this.correo.replace(/\_/gi,"883bdgs5");
          this.correo = this.correo.replace(/\-/gi,"ad76etr5");
    }

    async crearAdministrador(){
      try{
        const result = await this.afAuth.auth.createUserWithEmailAndPassword(this.email,this.password);
        if(result){
          this.encriptado();
          firebase.database().ref('/usuario/'+this.correo).set({
            correo : this.email,
            nombre : this.duen,
            tipo : 'administrador',
          });
          if(this.def != "/assets/img/defaultUser.png"){
            this.uploadImage();
          }
          this.clear();
        }
      }
      catch(e){
        this.password = '';
        this.email = '';
        this.texto = "Este correo ya se encuentra en uso. Intenta con otro";
        console.error(e);
      }
    }

    async crearDueno(){
      try{
        const result = await this.afAuth.auth.createUserWithEmailAndPassword(this.email,this.password);
        if(result){
          this.encriptado();
          firebase.database().ref('/usuario/'+this.correo).set({
            correo : this.email,
            nombre : this.duen,
            tipo : 'dueño',
          });
          if(this.def != "/assets/img/defaultUser.png"){
            this.uploadImage();
          }
        }
        this.clear();
      }
      catch(e){
        this.email = '';
        this.password = '';
        this.texto = "Este correo ya se encuentra en uso. Intenta con otro";
        console.error(e);
      }
      
    }

    async crearInquilino(){
      try{
        const result = await this.afAuth.auth.createUserWithEmailAndPassword(this.email,this.password);
        if(result){
          this.encriptado();
          firebase.database().ref('/usuario/'+this.correo).set({
            correo : this.email,
            nombre : this.duen,
            tipo : 'Inquilino',
          });
          if(this.def != "/assets/img/defaultUser.png"){
            this.uploadImage();
          }
        }
        this.clear();
      }
      catch(e){
        this.password = '';
        this.email = '';
        this.texto = "Este correo ya se encuentra en uso. Intenta con otro";
        console.error(e);
      }
    }

    addFoto(){
      this.camera.getPicture(this.optionsGallery).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.def = base64Image;
        this.filename = Math.floor(Date.now() / 1000);
        this.nombreImagen = `${this.filename}.jpg`
       }, (err) => {
        console.log(err);
      });
    }

    uploadImage(){
          let storageRef = firebase.storage().ref();
          const imageRef = storageRef.child(`${this.email}/ImagenesPerfil/${this.filename}.jpg`);
          imageRef.putString(this.def, firebase.storage.StringFormat.DATA_URL)
          .then((snapshot)=> {
            imageRef.getDownloadURL().then(res => {
              this.urlImagen = res;
              //this.events.publish('tienda:accion',aux1);
            });
        });
    }

    clear(){
      this.texto = '';
      this.password = '';
      this.duen = '';
      this.email = '';
      this.def = '/assets/img/defaultUser.png';
    }

  /*async AlertController() {
    const confirm = await this.alertCtrl.create({
      message: 'Los Datos no se podran cambiar luego de aceptar',
      buttons: [
        {
          text: 'Cambiar Datos',
          handler: () => {
            console.log('Cambiando datos');
          }
        },
        {
          text: 'Continuar',
          handler: () => {
            console.log('Continuar llenando datos');
          }
        }
      ]
    });
    await confirm.present();
  }*/

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Iniciando Sesion',
      duration: 500
    });
    return await loading.present();
  }
}
