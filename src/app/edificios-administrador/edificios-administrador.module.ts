import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EdificiosAdministradorPage } from './edificios-administrador.page';

const routes: Routes = [
  {
    path: '',
    component: EdificiosAdministradorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EdificiosAdministradorPage]
})
export class EdificiosAdministradorPageModule {}
