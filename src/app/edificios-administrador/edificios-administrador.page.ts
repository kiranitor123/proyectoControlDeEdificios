import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-edificios-administrador',
  templateUrl: './edificios-administrador.page.html',
  styleUrls: ['./edificios-administrador.page.scss'],
})
export class EdificiosAdministradorPage implements OnInit {

  constructor(private router: Router,private navCtrl:NavController) { }

  detalles(){
    this.navCtrl.navigateRoot(['tabs'])
  }
  add(){
    this.router.navigate(['edificio']);
  }
  ngOnInit() {
  }

}
