import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, RouterModule } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from '@ionic/angular';
import { Events } from '@ionic/angular';
import * as firebase from "firebase";
import { UsuariosServiceService } from '../services/usuarios-service.service';


var Usuario: any;
var userOnly: any ;
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  correo : any;
  contrasena : any;
  texto: any;
  ctrl : Boolean = true;
  ctrl1 : Boolean = false;
  correoReestablecimiento : any;
  correosemiencriptado : any;
  constructor(public navCtrl: NavController,public afAuth: AngularFireAuth,private router: Router,public loadingController: LoadingController,public events: Events, public userService: UsuariosServiceService) {
    
  }

  ngOnInit() {

  }

  async login(){
    this.semiencriptacion();

    await firebase.database().ref('/usuario/'+this.correosemiencriptado).once('value').then((snapshot) => {
    console.log("fire: ", snapshot.val());
    
      this.userService.ejemplo = snapshot.val();
      console.log("servicio: ", this.userService.ejemplo);
  
     // Usuario = snapshot.val();
      console.log("Lectura Realizada", this.userService.ejemplo);
    });

    const loading = await this.loadingController.create({
      message: 'Iniciando Sesion'
    });
    loading.present();

    /*this.correo = "ejqv1996.18@gmail.com";
    this.contrasena = "123456789";*/

    try{
      const result = await this.afAuth.auth.signInWithEmailAndPassword(this.correo,this.contrasena);
      if(result){
        if(this.userService.ejemplo.tipo == 'administrador')
        {
          this.router.navigate(['edificios-administrador']);
        }
        if(this.userService.ejemplo.tipo == 'inquilino')
        {
          this.router.navigate(['edificios-dueno-inquilino']);
        }
        if(this.userService.ejemplo.tipo == 'dueno')
        {
          this.router.navigate(['edificios-dueno-inquilino']);
        }
        loading.dismiss();
      }
    }
    catch(e){
      loading.dismiss();
      console.error(e);
      this.texto = "El correo o contraseña son incorrectos, por favor intentelo otra vez.";
    }
  }

  verificacion(){
    this.ctrl = false;
    this.ctrl1 = true;
    this.correo = '';
  }

  async reestablecimiento(){
    const loading = await this.loadingController.create({
      message: 'Enviando Correo De Reestablecimiento'
    });
    loading.present();
    var auth = firebase.auth();
    auth.sendPasswordResetEmail(this.correoReestablecimiento).then(function() {
      alert("El correo de restablecimiento ha sido enviado.");
    }).catch(function(error) {
      alert("Este correo no ha sido registrado.");
    });
    loading.dismiss();
  }

  semiencriptacion() {
    //Quitamos los caracteres especiales del correo
    this.correosemiencriptado = this.correo;
    this.correosemiencriptado = this.correosemiencriptado.replace(/\@/gi,"83nv6sl9");
    this.correosemiencriptado = this.correosemiencriptado.replace(/\./gi,"v88f73n3");
    this.correosemiencriptado = this.correosemiencriptado.replace(/\_/gi,"883bdgs5");
    this.correosemiencriptado = this.correosemiencriptado.replace(/\-/gi,"ad76etr5");
  }

  
}
