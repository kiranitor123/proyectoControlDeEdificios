import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdificiosDuenoInquilinoPage } from './edificios-dueno-inquilino.page';

describe('EdificiosDuenoInquilinoPage', () => {
  let component: EdificiosDuenoInquilinoPage;
  let fixture: ComponentFixture<EdificiosDuenoInquilinoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdificiosDuenoInquilinoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdificiosDuenoInquilinoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
