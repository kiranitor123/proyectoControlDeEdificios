import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-edificios-dueno-inquilino',
  templateUrl: './edificios-dueno-inquilino.page.html',
  styleUrls: ['./edificios-dueno-inquilino.page.scss'],
})
export class EdificiosDuenoInquilinoPage implements OnInit {

  constructor(private navCtrl:NavController) { }

  detalleInquilino(){
    this.navCtrl.navigateRoot(['tabs'])
  }
  ngOnInit() {
  }

}
