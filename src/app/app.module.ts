import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Camera } from '@ionic-native/camera/ngx';
import {ModalPagePageModule} from './modal-page/modal-page.module';


import * as firebase from "firebase";
import { ModalPagePage } from './modal-page/modal-page.page';

var config = {
  apiKey: "AIzaSyCTc9Tuh_i6WoOLtipNd6WHVAl6uJNH8ao",
  authDomain: "proyecto-control-edificio.firebaseapp.com",
  databaseURL: "https://proyecto-control-edificio.firebaseio.com",
  projectId: "proyecto-control-edificio",
  storageBucket: "proyecto-control-edificio.appspot.com",
  messagingSenderId: "31137474639"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,AngularFireModule.initializeApp(config),
    AngularFireAuthModule,ModalPagePageModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
