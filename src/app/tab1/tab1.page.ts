import { Component, OnInit } from '@angular/core';
import { ModalController, MenuController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';
import * as firebase from "firebase";
import { Events } from '@ionic/angular';
import { UsuariosServiceService } from '../services/usuarios-service.service';

var Edificio;


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  Edificio : any;
  tipo:any;
  constructor(private menu: MenuController,public modalController: ModalController,public events : Events,public service:UsuariosServiceService) 
  {
    this.tipo = this.service.ejemplo.tipo;
  }
  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  async detallesVivienda() {
    const modal = await this.modalController.create({
      component: ModalPagePage
    });
    await modal.present();
  }

  ngOnInit() {
  }
}
